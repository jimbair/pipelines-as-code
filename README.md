# pipelines-as-code

**pipelines-as-code** maintains the Gitlab CI pipeline definitions.

## Using container templates

Containers.yml is a generic set of steps for building containers in component repositories
The steps defined here can be automatically pulled into a component repository to
build and push containers within it, by including the following snippet
in the component repository:

```yaml
include:
  - project: "${CI_PROJECT_NAMESPACE}/pipelines-as-code"
    file:
      - '/.gitlab/containers.yml'
```

and adding the following to the stages of the gitlab file, to invoke the steps from this file:

```yaml
 stages:
   - build
```

The newly built container image will inherit the name of the repository, for example:
if the component repo is called my-awesome-component, then the resulting image will be as follows:
`quay.io/automotive-toolchain/my-awesome-component:latest`.
The container registry and namespace can be overriden with the `$CI_REGISTRY` variable

This .yml file expects a Containerfile called `Containerfile` in the root directory of the repository
in order to build the image.

### Multi-container repository

The default behavior for this template is to build a single container image from the repository, but
it's possible to use it for building more than one.

Let's say we have a repository called `my_containers` and we want to maintain there three different
container images:

- `base-image`
- `python-tools`
- `shell-tools`

We can create 3 directories with those names and a `Containerfile` inside each one of them. Then
we'll modify the `.gitlab-ci.yml` to have something like this:

```yaml
include:
  - project: "${CI_PROJECT_NAMESPACE}/pipelines-as-code"
    file:
      - '/.gitlab/containers.yml'
 stages:
   - build

container-build:
  rules:
    - when: never

build-containers:
  extends: .container-build
  parallel:
    matrix:
      - IMAGE_NAME: base-image
        CONTEXT: base-image
      - IMAGE_NAME: python-tools
        CONTEXT: python-tools
      - IMAGE_NAME: shell-tools
        CONTEXT: shell-tools
```

The variables `IMAGE_NAME` and `CONTEXT` don't nedd to be the same. The variable
`IMAGE_NAME` is the name for the name, while the `CONTEXT`is the name of the directory.

So, we could name the image `base-image` and the directory `base_image` or `container_1`,
ot whatever.


## Contributing

### pre-commit integration

With **pre-commit** you can test your code for small issues and run the appropriate
linters locally.

> :pushpin: To make use of this feature you will need to [install Pre-commit](#installing-pre-commit)
on your local machine.

To enable this, simply run `pre-commit install` from the project directory at any
stage after cloning and before committing. Now, every `git commit` run will be
followed by the hooks defined in [.pre-commit-config.yaml](.pre-commit-config.yaml).
Unless all tests have passed, the commit will be aborted.

#### Running hooks

The configured hooks will only run against the files that have been modified or added.
To allow testing on all files in the repository instead, you can use the following
command:

```bash
pre-commit run --all-files
```

and to run individual hooks:

```bash
pre-commit run <hook_id>
```

> :pushpin: Some hooks have local dependencies (e.g. markdownlint requires RubyGems)

If you wish to perform pre-commit testing but want to skip any specific test/hook
use SKIP on commit.
The SKIP environment variable is a comma separated list of hook ids as defined
in [.pre-commit-config.yaml](.pre-commit-config.yaml)

```bash
SKIP=<hook_id>, <hook_id> git commit -m "foo"
```

If you decide not to use pre-commit after enablement, `pre-commit uninstall` will
restore your hooks to the state prior to installation.
Alternatively, you can run your commit with `--no-verify`.

#### Installing Pre-commit

To install the pre-commit package manager, run the respective command for your preferred
package manager:

```bash
pip install pre-commit
```

```bash
brew install pre-commit
```

```bash
conda install -c conda-forge pre-commit
```
